/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.mock;

import ch.insign.playauth.authz.AccessControlEntry;
import ch.insign.playauth.authz.AccessControlList;
import ch.insign.playauth.authz.ObjectIdentity;
import ch.insign.playauth.authz.SecurityIdentity;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by serhii on 7/19/16.
 */
public class AccessControlListMock implements AccessControlList {

    private Set<AccessControlEntry> aces = new HashSet<>();

    @Override
    public Optional<AccessControlEntry> find(SecurityIdentity sid, ObjectIdentity oid) {
        return aces.stream()
                .filter(ace -> ace.getSid().equals(sid))
                .filter(ace -> ace.getOid().equals(oid))
                .findAny();
    }

    @Override
    public Optional<AccessControlEntry> find(AccessControlEntry.Identifier id) {
        return aces.stream()
                .filter(ace -> ace.getId().equals(id))
                .findAny();
    }

    @Override
    public List<AccessControlEntry> findAll() {
        return aces.stream().collect(toList());
    }

    @Override
    public Stream<AccessControlEntry> streamAll() {
        return aces.stream();
    }

    @Override
    public List<AccessControlEntry> findBySid(SecurityIdentity sid) {
        return aces.stream()
                .filter(ace -> ace.getSid().equals(sid))
                .collect(toList());
    }

    @Override
    public List<AccessControlEntry> findByOid(ObjectIdentity oid) {
        return aces.stream()
                .filter(ace -> ace.getOid().equals(oid))
                .collect(toList());
    }

    @Override
    public boolean contains(AccessControlEntry ace) {
        return aces.contains(ace);
    }

    @Override
    public AccessControlEntry put(AccessControlEntry ace) {
        find(ace.getSid(), ace.getOid())
                .ifPresent(old -> aces.remove(old));
        aces.add(ace);
        return ace;
    }

    @Override
    public void remove(AccessControlEntry ace) {
        find(ace.getSid(), ace.getOid())
                .ifPresent(old -> aces.remove(old));
    }

    @Override
    public void remove(AccessControlEntry.Identifier id) {
        aces.stream()
                .filter(ace -> ace.getId().equals(id))
                .findAny()
                .ifPresent(old -> aces.remove(old));
    }
}
