/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.event;

import javax.inject.Singleton;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toSet;

@Singleton
public class EventDispatcher {

    private Map<Class<? extends EventObject>, Set<EventListener>> listeners = new ConcurrentHashMap<>();

    public void dispatch(EventObject event) {
	    getListeners(event.getClass()).forEach(listener -> listener.handle(event));
    }

    public void addListener(Class<? extends EventObject> eventType, EventListener listener) {
	    listeners.computeIfAbsent(eventType, key -> new HashSet<>()).add(listener);
    }

	public void addSubscriber(EventSubscriber subscriber) {
		subscriber.getSubscribedEvents().forEach(eventType -> addListener(eventType, subscriber));
	}

	public void removeListener(EventListener listener) {
		listeners.values().forEach(entries -> entries.remove(listener));
	}

	public Collection<EventListener> getListeners(Class<? extends EventObject> eventType) {
		return listeners.entrySet().stream()
				.filter(e -> e.getKey().isAssignableFrom(eventType) || eventType.isAssignableFrom(e.getKey()))
				.flatMap(e -> e.getValue().stream())
				.collect(toSet());
	}

    public <T extends EventObject> Collection<EventListener> getListeners() {
	    return listeners.values().stream().flatMap(Collection::stream).collect(toSet());
    }

    public boolean hasListeners(Class<? extends EventObject> eventType) {
        return listeners.containsKey(eventType) || listeners.keySet().stream()
		        .anyMatch(key -> key.isAssignableFrom(eventType) || eventType.isAssignableFrom(key));
    }
}
