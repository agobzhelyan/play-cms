/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro;

import ch.insign.playauth.authc.AuthenticationListener;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.shiro.env.IniPlayShiroEnvironment;
import ch.insign.playauth.shiro.mgt.PlayShiroSecurityManager;
import ch.insign.playauth.shiro.subject.PlayShiroSubject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.authc.AbstractAuthenticator;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.mgt.AuthenticatingSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class PlayShiroApi {

    private final static Logger logger = LoggerFactory.getLogger(PlayShiroApi.class);

    private final PlayShiroSecurityManager securityManager;

    @Inject
    public PlayShiroApi(AuthenticationListener authenticationListener) {
        this.securityManager = initSecurityManager();
        AuthenticationListener authenticationListener1 = authenticationListener;

        Authenticator authenticator = ((AuthenticatingSecurityManager) securityManager).getAuthenticator();
        if (authenticator instanceof AbstractAuthenticator) {
            ((AbstractAuthenticator) authenticator).getAuthenticationListeners().add(authenticationListener);
        }
    }

    /**
     * Get the low level PlayShiroSecurityManager
     */
    public SecurityManager getSecurityManager() {
        return securityManager;
    }

    public Subject getSubject() {
        return getSubject(Http.Context.current.get());
    }

    public Subject getSubject(Http.Context ctx) {
        try {
            return SecurityUtils.getSubject();
        } catch (UnavailableSecurityManagerException e) {
            logger.trace("There is no accessible Subject, tyring to creating new Subject with optional Http.Context...", e);
            return createSubject(null, ctx);
        }
    }

    /**
     * Creates authenticated subject by the given principals or anonymous subject if no principals were given.
     */
    public Subject createSubject(Party party, Http.Context ctx) {
        PlayShiroSubject.Builder builder =
                new PlayShiroSubject.Builder(securityManager);

        if (party != null) {
            builder.principals(party.getPrincipals()).authenticated(true);
        }

        if (ctx != null) {
            builder.httpContext(ctx);
        }

        return builder.buildPlayShiroSubject();
    }

    private PlayShiroSecurityManager initSecurityManager() {
        IniPlayShiroEnvironment shiroEnvironment = new IniPlayShiroEnvironment();
        shiroEnvironment.init();

        return shiroEnvironment.getPlayShiroSecurityManager();
    }
}
