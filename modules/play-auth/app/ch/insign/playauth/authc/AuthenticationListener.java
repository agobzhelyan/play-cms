/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authc;

import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.event.PartyLoginFailureEvent;
import ch.insign.playauth.event.PartyLoginSuccessEvent;
import ch.insign.playauth.event.PartyLogoutEvent;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyManager;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.PrincipalCollection;

import javax.inject.Inject;

public class AuthenticationListener implements org.apache.shiro.authc.AuthenticationListener {

    private PartyManager pm;
    private EventDispatcher dispatcher;

    @Inject
    public AuthenticationListener(PartyManager pm, EventDispatcher dispatcher) {
        this.pm = pm;
        this.dispatcher = dispatcher;
    }

    @Override
    public void onSuccess(AuthenticationToken token, AuthenticationInfo info) {
        Party party = pm.findOneByPrincipals(info.getPrincipals());
        dispatcher.dispatch(new PartyLoginSuccessEvent(token, party));
    }

    @Override
    public void onFailure(AuthenticationToken token, AuthenticationException ae) {
        dispatcher.dispatch(new PartyLoginFailureEvent(token, ae));
    }

    @Override
    public void onLogout(PrincipalCollection principals) {
        Party party = pm.findOneByPrincipals(principals);
        dispatcher.dispatch(new PartyLogoutEvent(party));
    }

}
