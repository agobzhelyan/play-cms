/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.authz.SecurityIdentityRetrievalStrategy;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;

public class DefaultSecurityIdentityRetrievalStrategy implements SecurityIdentityRetrievalStrategy {

	@Override
	public SecurityIdentity getSecurityIdentity(Object authority) {

		if (authority == null) {
			return SecurityIdentity.ALL;
		}

		if (authority instanceof SecurityIdentity) {
			return (SecurityIdentity) authority;
		}

		if (authority instanceof Party) {
			return new SecurityIdentity((Party) authority);
		}

		if (authority instanceof PartyRole) {
			return new SecurityIdentity((PartyRole) authority);
		}

		throw new IllegalArgumentException("authority must be instance of Party or PartyRole or SecurityIdentity");
	}
}