/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.util.StringUtils;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Embeddable
public class ObjectIdentity implements Serializable, Comparable<ObjectIdentity> {

    public static final ObjectIdentity ALL = new ObjectIdentity("*", "*");
    public static final String CLASS_IDENTIFIER = "*";

    private String type;
    private String identifier;

	@Transient
	private Object source;

	@Transient
	private Map<Class<?>, Object> metadata = ImmutableMap.<Class<?>, Object>builder().build();

    protected ObjectIdentity() {

    }

	public ObjectIdentity(String type, String identifier, Object source, Map<Class<?>, Object> metadata) {
		this(type, identifier);
		Objects.requireNonNull(source);
		this.source = source;
		if (!metadata.isEmpty()) {
			this.metadata = ImmutableMap.<Class<?>, Object>builder().putAll(metadata).build();
		}
	}

    public ObjectIdentity(String type, String identifier) {
        if (!StringUtils.hasText(type)) {
            throw new IllegalArgumentException("type required");
        }
        if (!StringUtils.hasText(identifier)) {
            throw new IllegalArgumentException("identifier required");
        }

        this.type = type;
        this.identifier = identifier;
    }

    public String getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public boolean isClassIdentity() {
        return identifier.equals(CLASS_IDENTIFIER);
    }

    public ObjectIdentity asClassIdentity() {
        return isClassIdentity() ? this : new ObjectIdentity(type, CLASS_IDENTIFIER);
    }

	/**
	 * TODO: try to resolve source if not present
	 */
	public Optional<Object> getSource() {
		return Optional.ofNullable(source);
	}

	public Map<Class<?>, Object> getMetadata() {
		return metadata;
	}

	@SuppressWarnings("unchecked")
	public <T> Optional<T> getMetadata(Class<T> tag) {
		return Optional.ofNullable((T) metadata.get(tag));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ObjectIdentity that = (ObjectIdentity) o;

		if (!identifier.equals(that.identifier)) return false;
		if (!type.equals(that.type)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = type.hashCode();
		result = 31 * result + identifier.hashCode();
		return result;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", type)
                .append("identifier", identifier)
                .toString();
    }

    @Override
    public int compareTo(ObjectIdentity o) {
        return new CompareToBuilder()
            .append(getType(), o.getType())
            .append(getIdentifier(), o.getIdentifier())
            .toComparison();
    }
}
