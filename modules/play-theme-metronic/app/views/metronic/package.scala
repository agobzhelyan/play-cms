/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package views.html

import views.html.helper.FieldConstructor
import views.html.helper.FieldElements
import views.html.metronic.metronicFieldConstructor

/**
 * Contains template helpers, for example for generating HTML forms.
 */
package object metronic {

  /**
   * Metronic input structure.
   */
  implicit val metronicField = new FieldConstructor {
    def apply(elts: FieldElements) = metronicFieldConstructor(elts)
  }

}
