/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Add simple support for Java 8 Streams to JPA.
 *
 * @see {https://blogs.oracle.com/theaquarium/entry/jpa_and_java_se_8}
 */
public interface QueryStream<T> {

    /**
     * Return a stream of read-only objects.
     * <p>
     * The query will be executed against the Session,
     * and the resulting objects will not be tracked for changes.
     * The resulting objects are from the Session shared cache,
     * and must not be modified.
     * <p>
     * The stream should be closed to release resources.
     */
    Stream<T> getResultStream();

    /**
     * Applies a result stream to the given function and automatically releases resources.
     */
    <R> Optional<R> withResultStream(Function<Stream<T>, R> function);

    /**
     * Bind an argument to a named query parameter.
     */
    QueryStream<T> setParameter(String name, Object value);

}
