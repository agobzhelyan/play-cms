/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import ch.insign.commons.db.util.TestDatabase;
import org.junit.Rule;
import org.junit.Test;
import play.db.jpa.JPAApi;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EntityManagerTest {

    @Rule
    public TestDatabase db = new TestDatabase();

    @Test
    public void testShouldReuseEntityManagerWhenExecutingTransaction() {
        JPAApi api = db.jpa;
        boolean reused = api.withTransaction(entityManager -> {
            EntityManager fromContext = api.em();
            assertNotNull(fromContext);

            return fromContext == entityManager;
        });

        assertTrue(reused);
    }

}
