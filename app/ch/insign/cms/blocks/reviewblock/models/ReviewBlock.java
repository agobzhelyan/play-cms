/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.reviewblock.models;

import ch.insign.cms.blocks.reviewblock.controllers.ReviewFrontendController;
import ch.insign.cms.blocks.reviewblock.forms.ReviewSubmissionForm;
import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.ContentBlock;
import ch.insign.cms.models.PageBlock;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.*;


@Entity
@Table(name = "cms_review",  indexes = {
		@Index(name = "targetId_index", columnList = "targetId")
})
@DiscriminatorValue("ReviewBlock")
public class ReviewBlock extends ContentBlock {
	private final static Logger logger = LoggerFactory.getLogger(ReviewBlock.class);

    public static final Integer FIVE_STAR = 5;
    public static final Integer FOUR_STAR = 4;
    public static final Integer THREE_STAR = 3;
    public static final Integer TWO_STAR = 2;
    public static final Integer ONE_STAR = 1;

	public static ReviewBlockFinder<ReviewBlock> find = new ReviewBlockFinder<>(ReviewBlock.class);

	public enum ReviewStatus {
		ENABLED, DISABLED, CLOSED;
	}

	@Constraints.Required
	protected String targetId;

	protected ReviewStatus status = ReviewStatus.CLOSED;

	protected boolean loginRequired = true;

	protected Date createdOn;

	@ElementCollection
	@MapKeyColumn(name="RATING")
	@Column(name="RATINGS_COUNT")
	@CollectionTable(name="cms_review_ratings_stat", joinColumns=@JoinColumn(name="REVIEW_ID"))
	private Map<Integer,Integer> ratingsStatistic = new HashMap();

	protected Float avgRatings = 0f;

	protected Integer ratingsCount = 0;

	@OneToMany(mappedBy="review", cascade = CascadeType.ALL)
	List<ReviewSubmission> reviewSubmissions = new ArrayList<>();

	@Override
	public Html render() {
		ReviewSubmissionForm reviewSubmission = new ReviewSubmissionForm();
		reviewSubmission.setDisplayName(getSuggestedName());

		Form<ReviewSubmissionForm> form = SmartForm.form(ReviewSubmissionForm.class).fill(reviewSubmission);
		return ch.insign.cms.blocks.reviewblock.view.block.html.show.render(this, form);
	}

	public Html renderStatistic() {
		return ch.insign.cms.blocks.reviewblock.view.block.html.ratingsStat.render(this);
	}

	public Html renderGoogleReviewMicrodata() {
		return ch.insign.cms.blocks.reviewblock.view.google.html.ratingMicroformat.render(this);
	}

	@Override
	public Html editForm(Form editForm) {
		String backURL = Controller.request().getQueryString("backURL");
		return ch.insign.cms.blocks.reviewblock.view.block.html.edit.render(this, editForm, backURL, null);
	}

	@PrePersist
	public void onPrePersist() {
		createdOn = new Date();
	}

	@Override
	public void save() {
		getTarget().ifPresent(block -> block.markModified());
		super.save();
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public ReviewStatus getStatus() {
		return status;
	}

	public void setStatus(ReviewStatus status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getRatingsCount() {
		return ratingsCount;
	}

	public void setRatingsCount(Integer ratingsCount) {
		this.ratingsCount = ratingsCount;
	}

	public Float getAvgRatings() {
		return avgRatings;
	}

	public void setAvgRatings(Float avgRatings) {
		this.avgRatings = avgRatings;
	}

	public boolean isLoginRequired() {
		return loginRequired;
	}

	public void setLoginRequired(boolean loginRequired) {
		this.loginRequired = loginRequired;
	}

	public Optional<AbstractBlock> getTarget() {
        return Optional.ofNullable(AbstractBlock.find.byId(targetId));
    }

	public String getTargetTitle() {
        return getTarget()
                .filter(b -> b instanceof PageBlock)
                .map(b -> ((PageBlock) b).getPageTitle().get())
                .orElse("");
    }

	public List<ReviewSubmission> getApprovedSubmissions() {
		return ReviewSubmission.find
				.byReviewAndApproved(this, ReviewFrontendController.Sorting.DATE)
				.getResultList();
	}

	public List<ReviewSubmission> getReviewSubmissions() {
		return Collections.unmodifiableList(reviewSubmissions);
	}

	public void addSubmission(ReviewSubmission reviewSubmission) {
		if (reviewSubmissions.contains(reviewSubmission)) {
			return;
		}

		reviewSubmissions.add(reviewSubmission);
		if (reviewSubmission.isApproved) {
			includeSubmissionRating(reviewSubmission);
		}
	}

	public void removeSubmission(ReviewSubmission reviewSubmission) {
		if (!reviewSubmissions.contains(reviewSubmission)) {
			return;
		}

		if (reviewSubmission.isApproved) {
			excludeSubmissionRating(reviewSubmission);
		}
		reviewSubmissions.remove(reviewSubmission);
	}

	public void includeSubmissionRating(ReviewSubmission reviewSubmission) {
		if (reviewSubmissions.contains(reviewSubmission) && reviewSubmission.isApproved
				&& null != reviewSubmission.getRating()) {
			avgRatings = (reviewSubmission.getRating() + avgRatings * ratingsCount) / (ratingsCount + 1);
			incRatingsStatistic(reviewSubmission.getRating());
			ratingsCount++;
		}
	}

	public void excludeSubmissionRating(ReviewSubmission reviewSubmission) {
		if (reviewSubmissions.contains(reviewSubmission) && reviewSubmission.isApproved
				&& null != reviewSubmission.getRating()) {
			if (ratingsCount > 1) {
				avgRatings = (avgRatings * ratingsCount - reviewSubmission.getRating()) / (ratingsCount - 1);
				ratingsCount--;
			} else {
				ratingsCount = 0;
				avgRatings = 0f;
			}
			decRatingsStatistic(reviewSubmission.getRating());
		}
	}

	public String getSuggestedName() {
		if (ReviewFrontendController.getNameFromCookie().isPresent()) {
			return ReviewFrontendController.getNameFromCookie().get();
		}

		if (PlayAuth.isAuthenticated()) {
			return PlayAuth.getCurrentParty().getName();
		} else {
			return "";
		}
	}


    protected void incRatingsStatistic(@Nullable Integer rating) {
        if (null != rating) {
            setRatingsStatistic(rating, getRatingsStatistic(rating) + 1);
        }
    }

    protected void decRatingsStatistic(Integer rating) {
        if (null != rating && getRatingsStatistic(rating) > 0) {
            setRatingsStatistic(rating, getRatingsStatistic(rating) - 1);
        }
    }

    protected void setRatingsStatistic(Integer rating, Integer count) {
        if (null != rating && null != count) {
            ratingsStatistic.put(rating, count);
        }
    }

    public Integer getRatingsStatistic(Integer rating) {
        if (null == rating) {
            return null;
        }

        if (!ratingsStatistic.containsKey(rating)) {
            setRatingsStatistic(rating, 0);
        }
        return ratingsStatistic.get(rating);
    }

    public void refreshStatistic() {
        setRatingsStatistic(FIVE_STAR, 0);
        setRatingsStatistic(FOUR_STAR, 0);
        setRatingsStatistic(THREE_STAR, 0);
        setRatingsStatistic(TWO_STAR, 0);
        setRatingsStatistic(ONE_STAR, 0);

        ratingsCount = 0;
        Long ratingsSum = 0L;

        for (ReviewSubmission reviewSubmission: reviewSubmissions) {
            if (reviewSubmission.isApproved() && null != reviewSubmission.getRating()) {
                incRatingsStatistic(reviewSubmission.getRating());
                ratingsSum += reviewSubmission.getRating();
                ratingsCount++;
            }
        }

        if (ratingsCount > 0) {
            avgRatings = (float) 1.0 * ratingsSum / ratingsCount;
        }
    }

	public static class ReviewBlockFinder<T extends ReviewBlock> extends BlockFinder<T> {
		public ReviewBlockFinder(Class<T> entity) {
			super(entity);
		}

		@Nullable
		public T byTargetId(String id) {
			try {
				return query().andEquals("targetId", id).getSingleResult();
			} catch (NonUniqueResultException | NoResultException e) {
				logger.warn("Error retrieving ReviewBlock by targetId", e);
				return null;
			}
		}

	}

}
