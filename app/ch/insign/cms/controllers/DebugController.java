/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.Metrics;
import ch.insign.cms.permissions.aop.RequiresDebugPermission;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

/**
 * Created by bachi on 03.06.14.
 */
@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class DebugController extends Controller  {
	private static final Logger logger = LoggerFactory.getLogger(DebugController.class);

	private Metrics metrics;

    @Inject
    public DebugController(Metrics metrics) {
        this.metrics = metrics;
    }
    /**
     * Complete Metrics (http://metrics.codahale.com/) output as json.
     */
    @RequiresDebugPermission
    public Result metrics() {
        return ok(metrics.reportJson())
                .as("application/json");
        //.withHeaders("Cache-Control", "must-revalidate,no-cache,no-store");
    }

    /**
     * Complete Metrics (http://metrics.codahale.com/) output as json.
     */
    @RequiresDebugPermission
    public Result metricsReset() {
        metrics.init();
        return ok("Metrics registry reset.");
    }
}
