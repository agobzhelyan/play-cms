/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CustomerEmailTemplate;
import ch.insign.cms.permissions.CustomerEmailTemplatePermission;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuth;
import com.google.inject.Inject;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

@With(GlobalActionWrapper.class)
@Transactional
public class CustomerEmailTemplateController extends EmailTemplateController {

	@Inject
	public CustomerEmailTemplateController(MessagesApi messagesApi, FormFactory formFactory, JPAApi jpaApi) {
		super(messagesApi, formFactory, jpaApi);
	}

	public Result list() {
		PlayAuth.requirePermission(CustomerEmailTemplatePermission.BROWSE);

		return ok(ch.insign.cms.views.html.admin.customerEmailTemplate.list.render(CustomerEmailTemplate.find.all()));
	}

	@Override
	public Result add() {
		PlayAuth.requirePermission(CustomerEmailTemplatePermission.ADD);

		Form<CustomerEmailTemplate> form = formFactory.form(CustomerEmailTemplate.class);

		String outputRaw = ch.insign.cms.views.html.admin.customerEmailTemplate.add.render(form).toString();
		outputRaw = SecureForm.signForms(outputRaw);
		Html output = Html.apply(outputRaw);

		return ok(output);
	}

	@Override
	public Result doAdd() {
		PlayAuth.requirePermission(CustomerEmailTemplatePermission.ADD);

		Form<CustomerEmailTemplate> form = SmartForm
				.form(CustomerEmailTemplate.class)
				.bindFromRequest();

		if (form.hasErrors()) {
			return badRequest(SecureForm.signForms(ch.insign.cms.views.html.admin.customerEmailTemplate.add.render(form)));
		}

		form.get().save();

		flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "backend.customer.user.emailTemplate.message.add",
				form.get().getSubject()));

		return redirect(ch.insign.cms.controllers.routes.CustomerEmailTemplateController.list());
	}

	@Override
	public Result edit(String id) {
		CustomerEmailTemplate customerEmailTemplate = CustomerEmailTemplate.find.byId(id);

		if (customerEmailTemplate == null) {
			return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "backend.customer.email.not.found", id));
		}

		PlayAuth.requirePermission(CustomerEmailTemplatePermission.EDIT, customerEmailTemplate);

		Form<CustomerEmailTemplate> form = SmartForm
				.form(CustomerEmailTemplate.class)
				.fill(customerEmailTemplate);

		return ok(SecureForm.signForms(ch.insign.cms.views.html.admin.customerEmailTemplate.edit.render(form, customerEmailTemplate)));
	}

	@Override
	public Result doEdit(String id) {
		CustomerEmailTemplate customerEmailTemplate = CustomerEmailTemplate.find.byId(id);

		if (customerEmailTemplate == null) {
			return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "backend.customer.email.not.found", id));
		}

		PlayAuth.requirePermission(CustomerEmailTemplatePermission.EDIT, customerEmailTemplate);

		Form<CustomerEmailTemplate> form = SmartForm
				.form(CustomerEmailTemplate.class)
				.fill(customerEmailTemplate)
				.bindFromRequest();

		if (form.hasErrors()) {
			customerEmailTemplate.refresh();
			return badRequest(SecureForm.signForms(ch.insign.cms.views.html.admin.customerEmailTemplate.edit.render(form, customerEmailTemplate)));
		}

		form.get().save();
		JPA.em().flush();

		flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(),
                "backend.customer.user.emailTemplate.message.update", form.get().getSubject()));

		return redirect(ch.insign.cms.controllers.routes.CustomerEmailTemplateController.list());
	}

	public Result delete(String id) {
		CustomerEmailTemplate customerEmailTemplate = CustomerEmailTemplate.find.byId(id);

		if (customerEmailTemplate == null) {
			return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "backend.customer.email.not.found", id));
		}

		PlayAuth.requirePermission(CustomerEmailTemplatePermission.DELETE, customerEmailTemplate);

		flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(),
                "backend.customer.user.emailTemplate.message.delete", customerEmailTemplate.getSubject()));

		customerEmailTemplate.delete();

		return redirect(ch.insign.cms.controllers.routes.CustomerEmailTemplateController.list());
	}

}
