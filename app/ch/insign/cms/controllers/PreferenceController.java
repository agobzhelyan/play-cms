/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.controllers.actions.RequiresUser;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.preference.Preference;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.Optional;


@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class PreferenceController extends Controller {

    public static final String NAV_ITEMS_EXPAND = "NAV_ITEMS_EXPAND";

    private JPAApi jpaApi;

    @Inject
    public PreferenceController(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @RequiresUser
    public Result get(final String name) {
        Optional<String> maybeValue = PlayAuth.getCurrentParty()
                .getPreference(name.toUpperCase())
                .flatMap(Preference::getFirstOption);

        ObjectNode result = Json.newObject();
        maybeValue.ifPresent(v -> {
                result.put("value", v);
        });

        return ok(result);
    }

    @RequiresUser
    public Result save(final String name) {

        final Party party = PlayAuth.getCurrentParty();

        if (name.toUpperCase().equals(NAV_ITEMS_EXPAND)) {

            try {
                String[] ids = request().body().asFormUrlEncoded().get("ids[]");
                party.setPreferredOption(NAV_ITEMS_EXPAND, StringUtils.join(ids, ","));
            } catch (NullPointerException e) {
                party.setPreferredOption(NAV_ITEMS_EXPAND, "");
            }

            jpaApi.em().merge(party);
        }

        return ok();
    }
}
