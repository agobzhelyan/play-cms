/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.controller;

import ch.insign.cms.attributeset.model.Attribute;
import ch.insign.cms.attributeset.model.AttributeSet;
import ch.insign.cms.attributeset.model.OptionAttribute;
import ch.insign.cms.attributeset.model.OptionValue;
import ch.insign.cms.attributeset.views.html.backend.attributeAddEdit;
import ch.insign.cms.attributeset.views.html.backend.attributeList;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.commons.db.SmartForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.Optional;

@RequiresBackendAccess
@Transactional
public class AttributeController extends Controller {

    private final static Logger logger = LoggerFactory.getLogger(AttributeController.class);

    public Result index() {
        return ok(attributeList.render(Attribute.find.all()));
    }

    public Result showAdd(String klass) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Attribute attribute = (Attribute)Class.forName(klass).newInstance();
        Form form = SmartForm.form(attribute.getClass());
        return ok(SmartForm.signForms(attributeAddEdit.render(form, attribute)));
    }

    public Result doAdd(String klass) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        return ((Attribute)Class.forName(klass).newInstance()).doAdd();
    }

    public Result delete(String id){
        Attribute attr = Attribute.find.byId(id);
        if (attr != null){
            deleteAttributeRelations(attr);
            attr.delete();
        }
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.index());
    }

    /**
     * Delete attribute relations
     *
     * Before delete attribute we need delete all relations:
     *  - mapped values
     *  - attribute set
     *
     * See https://jira.insign.ch/browse/HTS-327
     */
    private void deleteAttributeRelations(Attribute attribute) {
        // Delete all mapped values
        Optional.of(attribute)
                .filter(attr -> attr instanceof OptionAttribute)
                .map(attr -> (OptionAttribute) attr)
                .map(OptionAttribute::getOptions)
                .orElse(new ArrayList<>()).stream()
                .flatMap(option -> option.getMappedValues().stream())
                .forEach(OptionValue::delete);

        // Delete attribute from attribute set
        AttributeSet.find.byAttribute(attribute)
                .forEach(attributeSet -> attributeSet.removeAttribute(attribute));
    }


    public Result showEdit(String id){
        Attribute attr = Attribute.find.byId(id);
        if (attr == null){
            // TODO: handle missing ids
            throw new IllegalArgumentException("Attribute with id "+id+" not found!");
        }
        Form form = SmartForm.form((Class<Attribute>)attr.getClass()).fill(attr);
        return ok(SmartForm.signForms(attributeAddEdit.render(form, attr)));
    }

    public Result doEdit(String id){
        Attribute attr = Attribute.find.byId(id);
        if (attr == null){
            // TODO: Handle exception
            throw new IllegalArgumentException("Attribute with id "+id+" not found!");
        }
        return attr.doEdit();
    }
}
