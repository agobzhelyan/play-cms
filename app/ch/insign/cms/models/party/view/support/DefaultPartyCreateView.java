/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models.party.view.support;

import ch.insign.cms.models.party.view.PartyCreateView;
import play.data.Form;
import play.twirl.api.Html;

public class DefaultPartyCreateView implements PartyCreateView {
    protected Form partyForm;

    @Override
    public PartyCreateView setForm(Form partyForm) {
        this.partyForm = partyForm;
        return this;
    }

    @Override
    public Html render() {
        return ch.insign.cms.views.html.admin.party.createForm.render(partyForm, null);
    }
}
