/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models.party.view.support;

import ch.insign.cms.models.party.view.PartyChangePasswordView;
import ch.insign.cms.models.party.view.PartyMenuItemsView;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.playauth.party.support.DefaultParty;
import com.google.inject.Inject;
import play.data.Form;
import play.twirl.api.Html;

public class DefaultPartyChangePasswordView implements PartyChangePasswordView {
    protected final PartyMenuItemsView partyMenuItemsView;
    protected DefaultParty party;
    protected Form passwordForm;

    @Inject
    public DefaultPartyChangePasswordView(PartyMenuItemsView partyMenuItemsView) {
        this.partyMenuItemsView = partyMenuItemsView;
    }

    @Override
    public PartyChangePasswordView setParty(DefaultParty party) {
        this.party = party;
        return this;
    }

    @Override
    public PartyChangePasswordView setForm(Form passwordForm) {
        this.passwordForm = passwordForm;
        return this;
    }

    @Override
    public Html render() {
        return ch.insign.cms.views.html.admin.party.passwordForm.render(
                new AdminContext(),
                party,
                passwordForm,
                partyMenuItemsView.setParty(party).render());
    }
}
