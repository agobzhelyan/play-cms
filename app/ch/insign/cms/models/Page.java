/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.MString;

import java.util.List;

/**
 * The page interface defines the methods that can be used in main templates. This allows to use these templates
 * from cms pages as well as non-cms pages.
 *
 * @author bachi
 *
 */
public interface Page {

    public MString getPageTitle();
    public MString getMetaTitle();
    public MString getPageDescription();
    public List<PageBlock> getSubPages();
    public String alternateLanguageUrl(String key);
    public String getKey();
}
