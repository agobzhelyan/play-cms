/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.email;

import ch.insign.cms.models.CmsConfiguration;

import javax.inject.Inject;

/**
 * The default implementation of the email service's configuration
 *
 * This implementation relies on the CMS configuration for loading it's values
 */
public class DefaultEmailServiceConfiguration implements EmailServiceConfiguration {

    private final String smtpHost;

    private final int smtpPort;

    private final String defaultSenderEmail;

    private final boolean startTlsEnabled;

    private final boolean startTlsRequired;

    @Inject
    public DefaultEmailServiceConfiguration(CmsConfiguration config) {
        smtpHost = config.getEmailSmtpHost();
        smtpPort = config.getEmailSmtpPort();
        defaultSenderEmail = config.getEmailSender();
        startTlsEnabled = config.getEmailSmtpTlsEnabled();
        startTlsRequired = config.getEmailSmtpTlsRequired();
    }

    @Override
    public String getSmtpHost() {
        return smtpHost;
    }

    @Override
    public int getSmtpPort() {
        return smtpPort;
    }

    @Override
    public String getDefaultSenderEmail() {
        return defaultSenderEmail;
    }

    @Override
    public boolean getStartTLSEnabled() {
        return startTlsEnabled;
    }

    @Override
    public boolean getStartTLSRequired() {
        return startTlsRequired;
    }
}
