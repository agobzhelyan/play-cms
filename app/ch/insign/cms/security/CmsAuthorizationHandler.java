/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.security;

import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.controllers.AuthController;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.PageBlock;
import com.google.inject.Inject;
import play.db.jpa.JPA;
import ch.insign.playauth.authz.AuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;

import java.util.Map;


public class CmsAuthorizationHandler implements AuthorizationHandler {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CmsAuthorizationHandler.class);

    private JPAApi jpaApi;

    @Inject
    public CmsAuthorizationHandler(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @Override
    public Result onUnauthorized(Context ctx, AuthorizationException e) {
	    try {
		    return jpaApi.withTransaction(() -> Results.forbidden(PageBlock.find.byKey(ErrorPage.KEY_FORBIDDEN).render()));
	    } catch (Throwable throwable) {
            logger.info("Error page \"forbidden\" is not created yet.");
		    return Results.forbidden();
	    }
    }

    @Override
    public Result onUnauthenticated(Context ctx, AuthorizationException e) {
        logger.info("Trying to acces secured page without being authenricated. Path = " + ctx.request().path());

        // save flash messages before redirect to another url
        // because data stored in the Flash scope are available to the next request only.
        for(Map.Entry<String, String> entry : ctx.flash().entrySet()) {
            ctx.flash().put(entry.getKey(), entry.getValue());
        }

        if (AuthController.hasJustLoggedOut(ctx)) {
            logger.debug("A user has just logged out, redirecting to /");
            return Results.redirect("/");
        }

        ctx.flash().put(AuthController.BACK_URL, ctx.request().uri());

        if (ctx.request().path().startsWith(CMS.getConfig().backendPath())) {
            // Redirect the user to the admin login screen...
            return Results.redirect(ch.insign.cms.controllers.routes.AdminController.adminLogin());
        } else {
            AuthController.setAsJustUnauthorized(ctx);
            return Results.redirect("/");
        }
    }

}
