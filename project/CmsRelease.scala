import com.typesafe.sbt.GitPlugin.autoImport._
import sbt._
import sbt.Keys._
import sbtrelease._
import sbtrelease.ReleasePlugin.autoImport._
import sbtrelease.ReleaseStateTransformations.{setReleaseVersion => _, _}
import de.heikoseeberger.sbtheader.HeaderPlugin
import sbtrelease.Utilities.{Yes, extractDefault}

object CmsRelease extends AutoPlugin {

  override def requires = HeaderPlugin

  /* Helpers */

  private def vcs(st: State): Vcs = {
    Project.extract(st).get(releaseVcs).getOrElse(sys.error("Aborting release. Working directory is not a repository of a recognized VCS."))
  }

  private def setVersion(selectVersion: Versions => String): ReleaseStep = { st: State =>
    val vs = st.get(ReleaseKeys.versions).getOrElse(sys.error("No versions are set! Was this release part executed before inquireVersions?"))
    val selected = selectVersion(vs)

    st.log.info("Setting version to '%s'." format selected)
    val useGlobal =Project.extract(st).get(releaseUseGlobalVersion)

    reapply(Seq(
      if (useGlobal) version in ThisBuild := selected
      else version := selected
    ), st)
  }

  /* Custom release steps */

  private lazy val setReleaseVersion: ReleaseStep = setVersion(_._1)

  private lazy val setNextVersion: ReleaseStep = setVersion(_._2)

  private lazy val pushTags = ReleaseStep(
    action = { st: State =>
      val defaultChoice = extractDefault(st, "y")
      val vc = vcs(st)

      defaultChoice orElse SimpleReader.readLine("Push tags to the remote repository (y/n)? [y] ") match {
        case Yes() | Some("") =>
          // Git outputs to standard error, so use a logger that redirects stderr to info
          vc.cmd("push", "--tags", "origin") !! vc.stdErrorToStdOut(st.log)
        case _ => st.log.warn("Remember to push tags yourself!")
      }
      st
    },
    check = { st: State =>
      val defaultChoice = extractDefault(st, "n")

      st.log.info("Checking remote [origin] ...")
      if (vcs(st).checkRemote("origin") ! st.log != 0) {
        defaultChoice orElse SimpleReader.readLine("Error while checking remote. Still continue (y/n)? [n] ") match {
          case Yes() => // do nothing
          case _ => sys.error("Aborting the release!")
        }
      }

      st
    }
  )

  val checkHeaders: ReleaseStep =
    releaseStepCommand("checkHeaders")

  val settings : Seq[Setting[_]] = Seq(

    // sbt-git
    // Derive the version of the project the most recent git tag

    git.useGitDescribe := true,
    git.baseVersion := "0.0.0",
    git.uncommittedSignifier := None,
    git.gitTagToVersionNumber := { v => {
      val ver = Version(v drop 1) // drop prefix "v" from version string
      val isSnapshot = git.gitCurrentTags.value.isEmpty || git.gitUncommittedChanges.value
      val hasSnapshotQualifier = ver.flatMap(_.qualifier).exists(_ == "-SNAPSHOT")

      if (isSnapshot && !hasSnapshotQualifier) {
        ver.map(_.bump(sbtrelease.Version.Bump.default).asSnapshot.string)
      } else {
        ver.map(_.string)
      }
    }},

    // sbt-release
    // Release the project by running tests and publishing artifacts with bumped up version

    // Force GIT as project vcs
    releaseVcs := Some(sbtrelease.Git.mkVcs(baseDirectory.value)),

    releasePublishArtifactsAction := publish.value,

    releaseProcess := Seq(
      checkSnapshotDependencies,
      inquireVersions,
      checkHeaders,
      runClean,
      runTest,
      setReleaseVersion,
      tagRelease,
      publishArtifacts,
      pushTags
    )
  )
}
