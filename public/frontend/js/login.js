(function($) {
    var loginModal = $('#loginModal');

    $(document).on('submit', '#loginModal form', function() {
        var form = $(this);

        loginModal.fadeOut({
            'duration' : 300,
            'done' : function() {
                $('div.login-error', loginModal).addClass("hide");
                $('div.group-email', loginModal).removeClass("has-error");
                $('.group-email .help-block', loginModal).addClass("hide").html("");
                $('div.group-password', loginModal).removeClass("has-error");
                $('.group-password .help-block', loginModal).addClass("hide").html("");

                form.ajaxSubmit({
                    dataType : 'json',
                    success : function(response) {
                        if (response.status === 'redirect') {
                            window.location.href = response.content;
                        } else if (response.status === 'error') {
                            $('input[name=password]').val("");

                            if(response.errors) {
                                for(index in response.errors) {
                                    if (index === "") {
                                        $('div.login-error', loginModal)
                                            .html(response.errors[index].join("<br />"))
                                            .removeClass("hide");
                                    } else {
                                        $("div.group-"+index, loginModal).addClass("has-error");
                                        $(".group-"+index+" .help-block.errors", loginModal).html(response.errors[index].join("<br />"));
                                        $(".group-"+index+" .help-block.errors", loginModal).removeClass("hide");
                                    }
                                }
                            }
                            loginModal.fadeIn({
                                'duration' : 300
                            });
                        } else {
                            window.location.reload(true);
                        }
                    }
                });
            }
        })
        return false;
    });


})(jQuery);