(function($) {

	$( document ).ready(function() {
		if ($("#selectRowValue").length > 0) {
			$("#selectRowValue").change(function(e) {
				var countVal = 0;
				var term = "";

				if ($("select#selectRowValue").val() !== "") {
					countVal = $("select#selectRowValue").val()
				}

				if ($("#term").length > 0  && $("#term").val() !== "") {
					term = $("#term").val();
				}

				window.location = jsRoutes.ch.insign.cms.blocks.reviewblock.controllers.ReviewBackendController.list(term, 1, countVal).url;

			});
		}
	});
})(jQuery);
