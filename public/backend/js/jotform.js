jQuery(document).ready(function($) {
    var $jotFormModal = $("#modal-add-jotform");
    var $successMessage = $jotFormModal.find("#jotform-succes-message");
    var $errorMessage = $jotFormModal.find("#jotform-error-message");

    $(document).on('click', "#add-jotform", function(e) {
        e.preventDefault();
        $successMessage.hide();
        $errorMessage.hide();
        $jotFormModal.load($(this).attr("href"), function() {
            $(this).modal({show: true, modalOverflow: true});
            $(this).find(".btnAddJotform").click(function(){
                bindAddJotFormButton($(this), $jotFormModal);
            });
        });
    });

    function bindAddJotFormButton(button, dialog) {
        var addUrl = $(button).attr("data-add-url");
        var tableContentUrl = $(button).attr("data-table-content-url");
        var $successMessage = dialog.find("#jotform-succes-message");
        var $errorMessage = dialog.find("#jotform-error-message");
        var $tableContent = $("#tableBodyContent");

        $.post(addUrl, function(response) {
            if(response.status == 'ok') {
                $successMessage.show();
                $errorMessage.hide();
                if (typeof $tableContent != "undefined") {
                    $tableContent.load(tableContentUrl);
                }
            } else if(response.status == 'error') {
                $errorMessage.html(response.message).show();
                $successMessage.hide();
            }
        }).fail(function() {
            $errorMessage.show();
            $successMessage.hide();
        });
        return false;

    }
});

